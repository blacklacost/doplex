variable "app_name" {
  description = "Название приложения" 
}

variable "app_type" {
  description = "Тип приложения (то есть stage, prod, dev)"
}

variable "buildpacks" {
  type = "list"
  default = []
}

variable "region" {
  description = "Регион Heroku. Может быть eu или us"
  default = "eu"
}

variable "postgresql_plan" {
  default = "heroku-postgresql:hobby-dev"
}

variable "mailgun_plan" {
  default = "mailgun:starter"
}

variable "sentry_plan" {
  default = "sentry:f1"
}

variable "redis_plan" {
  default = "heroku-redis:hobby-dev"
}

resource "heroku_app" "app" {
  name   = "${var.app_name}${var.app_type == "prod" ? "" : "-${var.app_type}"}"
  region = var.region
  buildpacks = var.buildpacks
}

module "postgresql" {
  source = "./addons/postgresql"

  app_name = heroku_app.app.name 
  addon_plan = var.postgresql_plan
}

module "mailgun" {
  source = "./addons/mailgun"

  app_name = heroku_app.app.name 
  addon_plan = var.mailgun_plan
}

module "sentry" {
  source = "./addons/sentry"

  app_name = heroku_app.app.name
  addon_plan = var.sentry_plan
}

module "redis" {
  source = "./addons/redis"

  app_name = heroku_app.app.name
  addon_plan = var.redis_plan
}

module "cookiecutter-django" {
  source = "./cookiecutter-django"

  app_id = heroku_app.app.id
  app_name = heroku_app.app.name
  redis_url = module.redis.redis_url
}
