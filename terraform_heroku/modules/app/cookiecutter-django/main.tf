variable "app_id" {}
variable "redis_url" {}
variable "app_name" {}

module "data_gen" {
  source = "./data_gen"
}


resource "heroku_config" "cookiecutter-django" {
  vars = {
    PYTHONHASHSEED = "random"
    WEB_CONCURRENCY = "4"
    DJANGO_DEBUG = "False"
    DJANGO_SETTINGS_MODULE = "config.settings.production"
    DJANGO_ALLOWED_HOSTS = "${var.app_name}.herokuapp.com"
    DJANGO_SECRET_KEY = (contains(keys(data.heroku_app.app.config_vars), "DJANGO_SECRET_KEY") ? data.heroku_app.app.config_vars.DJANGO_SECRET_KEY : module.data_gen.secret_key)
    DJANGO_ADMIN_URL = (contains(keys(data.heroku_app.app.config_vars), "DJANGO_ADMIN_URL") ? data.heroku_app.app.config_vars.DJANGO_ADMIN_URL : module.data_gen.admin_url)
    CELERY_BROKER_URL = var.redis_url
  }
}

resource "heroku_app_config_association" "cookicutter-django" {
  app_id = var.app_id
  vars = heroku_config.cookiecutter-django.vars
}

data "heroku_app" "app" {
  name = var.app_name
}
