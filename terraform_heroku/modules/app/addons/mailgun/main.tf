variable "app_name" {}
variable "addon_plan" {}

module "addon" {
  source = "../addon"

  app_name = var.app_name
  addon_plan = var.addon_plan
  addon_name = "${var.app_name}-mailgun"
}
