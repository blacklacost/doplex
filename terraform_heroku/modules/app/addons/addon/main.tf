variable "app_name" {}
variable "addon_plan" {}
variable "addon_name" {}

resource "heroku_addon" "addon" {
  app  = var.app_name
  plan = var.addon_plan
  name = var.addon_name
}